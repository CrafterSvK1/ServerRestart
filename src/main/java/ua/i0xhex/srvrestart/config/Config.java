package ua.i0xhex.srvrestart.config;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;

import com.cronutils.model.Cron;
import com.cronutils.model.CronType;
import com.cronutils.model.definition.CronDefinition;
import com.cronutils.model.definition.CronDefinitionBuilder;
import com.cronutils.parser.CronParser;

import ua.i0xhex.srvrestart.ServerRestart;
import ua.i0xhex.srvrestart.action.Action;
import ua.i0xhex.srvrestart.action.ActionParser;
import ua.i0xhex.srvrestart.config.AbstractConfig;

public class Config extends AbstractConfig {
    
    private List<Cron> cronList;
    private Map<Integer, List<Action>> actionMap;
    
    public Config(ServerRestart plugin) {
        super(plugin, "config.yml", true);
        
        cronList = new ArrayList<>();
        CronDefinition definition = CronDefinitionBuilder.instanceDefinitionFor(CronType.QUARTZ);
        for (String cronTime : config.getStringList("schedule")) {
            try {
                Cron cron = new CronParser(definition).parse(cronTime);
                cronList.add(cron);
            } catch (Exception ex) {
                Bukkit.getLogger().warning(String.format("" +
                        "[ServerRestart] Cron time is invalid: `%s`.", cronTime));
            }
        }
        
        actionMap = new LinkedHashMap<>();
        ConfigurationSection actionListSection = config.getConfigurationSection("actions");
        for (String secondsStr : actionListSection.getKeys(false)) {
            int seconds = Integer.parseInt(secondsStr);
            List<Action> actionList = new ArrayList<>();
            for (String actionData : actionListSection.getStringList(secondsStr))
                actionList.add(ActionParser.parseAction(actionData));
            actionMap.put(seconds, actionList);
        }
    }
    
    // getters
    
    public List<Cron> getCronList() {
        return cronList;
    }
    
    public Map<Integer, List<Action>> getActionMap() {
        return actionMap;
    }
}
